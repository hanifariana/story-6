from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import models
from .views import indexStory6
from .models import Kegiatan, Peserta
from .models import *
from .forms import *
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story6UnitTest(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200) 

    def test_model_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "nangis")
        Peserta.objects.create(nama_peserta = "hanifa")
        counter = Kegiatan.objects.all().count()
        counter += Peserta.objects.all().count()
        self.assertEqual(counter, 2)
    
    def test_model_kegiatan_2(self):
        Kegiatan.objects.create(nama_kegiatan = "tidur")
        kegiatan  = Kegiatan.objects.get(nama_kegiatan = "tidur")
        self.assertEqual(str(kegiatan), "tidur")

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'jadwal.html')

    def test_hallo(self):
        request = HttpRequest()
        response = indexStory6(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hallo", html_response)

    def test_tambahkan_kegiatan_baru(self):
        request = HttpRequest()
        response = indexStory6(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Tambahkan Kegiatan Baru", html_response)

    def test_form_valid(self):
        data = {'nama_kegiatan':"abc"}
        kegiatan_form = KegiatanCreateForm(data=data)
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"abc")

    def test_form_post(self):
        test_str = 'abc'
        response_post = Client().post('', {'nama_kegiatan':"abc"})
        self.assertEqual(response_post.status_code,200)
        kegiatan_form = KegiatanCreateForm(data={'nama_kegiatan':test_str})
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"abc")
