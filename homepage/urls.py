from django.contrib import admin
from django.urls import path, include

# from .views import addActivity
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.indexStory6, name = 'indexStory6'),
]
