# Generated by Django 3.1.2 on 2020-10-23 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0004_remove_kegiatan_deskripsi_kegiatan'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kegiatan',
            name='nama_kegiatan',
            field=models.CharField(max_length=65),
        ),
    ]
